const {HttpRoute, HttpMethods} = global.SnappFramework;

module.exports = function (self) {
  self.route(HttpRoute(HttpMethods.post, this.Url, this.function));
};
