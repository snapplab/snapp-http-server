const {HttpRoute, HttpMethods} = global.SnappFramework;

module.exports = function (self) {
  self.route(HttpRoute(HttpMethods.put, this.Url, this.function));
};
