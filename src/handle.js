module.exports = function (self) {
  let interaction = this.HttpInteraction;

  for (let i in self.forwards) {
    let forward = self.forwards[i];
    if (interaction.request.method.is(forward.method) &&
        interaction.request.url.startsWith(forward.url)) {
      return forward.handler(interaction);
    }
  }

  for (let i in self.routes) {
    let route = self.routes[i];
    if (interaction.request.method.is(route.method) &&
        interaction.request.url.matches(route.url)) {
      return route.handler(interaction);
    }
  }

  interaction.notFound();
};
