const {HttpRoute, HttpMethods} = global.SnappFramework;

module.exports = function (self) {
  let baseUrl = this.Url;
  let forwardTo = this.HttpServer;
  self.forward(HttpRoute(baseUrl, HttpMethods.any, function () {
    let interaction = this.HttpInteraction;
    interaction.request.url = interaction.request.url.relativeTo(baseUrl);
    forwardTo.handle(interaction);
  }));
};
