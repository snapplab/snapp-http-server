const {HttpRoute, HttpMethods} = global.SnappFramework;

module.exports = function (self) {
  self.route(HttpRoute(HttpMethods.delete, this.Url, this.function));
};
