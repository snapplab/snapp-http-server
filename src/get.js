const {HttpRoute, HttpMethods} = global.SnappFramework;

module.exports = function (self) {
  self.route(HttpRoute(HttpMethods.get, this.Url, this.function));
};
