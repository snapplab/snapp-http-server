module.exports = function (self) {
  let desc = [self.routes.length, `route${self.routes.length === 1 ? '' : 's'}`];
  if (self.listeningOnPort) {
    desc.push(self.listeningOnPort)
  }
  return desc.join(' ');
};
