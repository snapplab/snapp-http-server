const http = require('http');

const {
  HttpInteraction, HttpRequest, HttpResponse
} = global.SnappFramework;

module.exports = function (self) {
  let requestHandler = (request, response) => {
    let interaction = HttpInteraction(
      HttpRequest(request),
      HttpResponse(response)
    );

    self.handle(interaction);
  };

  self.forwards = [];
  self.routes = [];
  self.listeningOnPort = null;
  self.server = http.createServer(requestHandler);
};
