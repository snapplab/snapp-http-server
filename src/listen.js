module.exports = function (self) {
  let port = this.Port;
  self.server.listen(port.number);
  self.listeningOnPort = port;
};
